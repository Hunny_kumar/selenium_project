import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Alert {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","C:\\Users\\H kumar\\Downloads\\Driver\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.get("https://demoqa.com/alerts");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		//driver.manage().window().maximize();
		driver.manage().window().maximize();
		  // This step will result in an alert on screen
		    driver.findElement(By.id("alertButton")).click();
		    org.openqa.selenium.Alert simpleAlert =driver.switchTo().alert();
		    ((org.openqa.selenium.Alert) simpleAlert).accept();
		
		
//		boolean b=driver.findElement(By.id("alertButton")).isDisplayed();
//		if(true)
//		{
//			driver.findElement(By.id("alertButton")).click();
//			WebDriverWait wait=new WebDriverWait(driver,10);
//			org.openqa.selenium.Alert alert= wait.until(ExpectedConditions.alertIsPresent());
//			System.out.println("Hi");
//			
//			System.out.println(alert.getText());
//			alert.accept();
//			
//		}
//		WebElement c=driver.findElement(By.id("timerAlertButton"));
//		if(c.isDisplayed()==true)
//		{
//			c.click();
//			WebDriverWait wait=new WebDriverWait(driver,10);
//			org.openqa.selenium.Alert alert= wait.until(ExpectedConditions.alertIsPresent());
//			System.out.println("Hi");
//			System.out.println(alert.getText());
//			alert.accept();
//		}
//		
//		WebElement d=driver.findElement(By.id("confirmButton"));
//		if(d.isDisplayed()==true)
//		{
//			d.click();
//			WebDriverWait wait=new WebDriverWait(driver,10);
//			org.openqa.selenium.Alert alert= wait.until(ExpectedConditions.alertIsPresent());
//			System.out.println("Hi");
//			
//			
//			System.out.println(alert.getText());
//			alert.dismiss();
//		}
//		
//		WebElement e=driver.findElement(By.id("promtButton"));
//		if(e.isDisplayed()==true)
//		{
//			e.click();
//			WebDriverWait wait=new WebDriverWait(driver,10);
//			org.openqa.selenium.Alert alert= wait.until(ExpectedConditions.alertIsPresent());
//			System.out.println("Hi");
//			
//			
//			System.out.println(alert.getText());
//			alert.sendKeys("Hi Hunny kumar");
//			alert.accept();
//		}
		
	}

}
